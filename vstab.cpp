#include "cv.h"
    #include "cvaux.hpp"
    #include "highgui.h"
    #include <iostream>
    #include <stdio.h>



   using namespace std;
    using namespace cv;


int main( int argc, char** argv )
{
cvNamedWindow( “stabilize”, CV_WINDOW_AUTOSIZE );
//CvCapture* capture = cvCreateFileCapture( const char* taskvideo );

IplImage* frame = 0;
IplImage* frame1 = 0;
CvCapture* capture = cvCaptureFromAVI("taskvideo.avi");
frame = cvQueryFrame( capture );

 // Clone the frame to have an identically sized and typed copy
frame1 = cvCloneImage( frame );

while(frame = cvQueryFrame( capture ))
{

    cvCopy( frame , frame1);
}

Mat outImg = frame.clone();


/*
IplImage *frame = cvQueryFrame(capture); //to read properties of frame.
IplImage *frame1 = NULL;


while (1){
    if(frame1)
        frame = cvCloneImage(frame1); // copy image to allow grabbing next frame
    frame1 = cvQueryFrame(capture); //read next frame
    if(!frame1) break; //if frame cannot be read, EOF so break from loop
}

*/

cvShowImage( “stabilize”, frame );
cvShowImage( “stabilize”, frame1 );





bool run = true;

int key =0;


    key = cvWaitKey(10);


       int threshold=60;
    //vector<KeyPoint> keypoints=12;


Mat gray;
Mat gray1;
cvtColor(frame, gray, CV_BGR2GRAY);
cvtColor(frame1, gray1, CV_BGR2GRAY);
/*FAST(gray,keypoints,threshold,true);
FAST(gray2, keypoints, threshold, true);

img1 = cv2.drawKeypoints(gray, keypoints, color=(255,0,0));
img2 =cv2.drawkeypoints(gray2, keypoints, color=(255,0 ,0));

*/


// detecting keypoints
FastFeatureDetector detector(12);
vector<KeyPoint> keypoints1, keypoints2;
detector.detect(gray, keypoints1);
detector.detect(gray1, keypoints2);

// computing descriptors
SurfDescriptorExtractor extractor;
Mat descriptors1, descriptors2;
extractor.compute(gray, keypoints1, descriptors1);
extractor.compute(gray1, keypoints2, descriptors2);

// matching descriptors
BruteForceMatcher<L2<float> > matcher;
vector<DMatch> matches;
matcher.match(descriptors1, descriptors2, matches);

// drawing the results
namedWindow("matches", 1);
Mat img_matches;
drawMatches(gray, keypoints1, gray1, keypoints2, matches, img_matches);
imshow("matches", img_matches);
waitKey(0);

    imshow("stabilize", gray);
    imshow("stabilize", gray1);

/*//affine
keypoints1[12], keypoints2[12];
warp_mat = getAffineTransform( keypoints1, keypoints2 );
warpAffine( keypoint1, warp_dst, warp_mat, warp_dst.size() );



    rigid_mat = estimateRigidTransform(gray, gray1, false);

    warpAffine(gray, gray1, rigid_mat, gray.size(), INTER_NEAREST|WARP_INVERSE_MAP, BORDER_CONSTANT);
*/

int frameW, frameH;
frameW = gray.cols;
frameH = gray.rows;

Mat M = estimateRigidTransform(frame,frame1,0)
warpAffine(frame,outImg,M,Size(frameW,frameH),INTER_NEAREST|WARP_INVERSE_MAP)
    // ---------------------------------------------------------------------------//


//-----------------------------------------------------------------------

IplImage* out = cvCreateImage(
cvGetSize(frame),
IPL_DEPTH_8U,
3
);
// Do the smoothing
//
cvSmooth( frame, out, CV_GAUSSIAN, 3, 3 );
// Show the smoothed image in the output window
//
cvShowImage( “stabilise”, out );
// Be tidy
//
cvReleaseImage( &out );
// Wait for the user to hit a key, then clean up the windows
//
cvWaitKey( 0 );
cvDestroyWindow( “stabilise” );

return 0;
}
